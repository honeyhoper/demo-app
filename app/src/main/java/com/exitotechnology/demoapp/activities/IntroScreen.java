package com.exitotechnology.demoapp.activities;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import com.github.paolorotolo.appintro.AppIntro;
import com.exitotechnology.demoapp.R;
import com.exitotechnology.demoapp.utils.LocaleHelper;
import com.exitotechnology.demoapp.constant.ConstantValues;
import com.exitotechnology.demoapp.app.MyAppPrefsManager;
import com.exitotechnology.demoapp.fragments.Intro_Slide_1;
import com.exitotechnology.demoapp.fragments.Intro_Slide_2;
import com.exitotechnology.demoapp.fragments.Intro_Slide_3;
import com.exitotechnology.demoapp.fragments.Intro_Slide_4;
import com.exitotechnology.demoapp.fragments.Intro_Slide_5;
public class IntroScreen extends AppIntro {
    MyAppPrefsManager myAppPrefsManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myAppPrefsManager = new MyAppPrefsManager(IntroScreen.this);
        addSlide(new Intro_Slide_1());
        addSlide(new Intro_Slide_2());
        addSlide(new Intro_Slide_3());
        addSlide(new Intro_Slide_4());
        addSlide(new Intro_Slide_5());
        showStatusBar(false);
        showSkipButton(true);
        setProgressButtonEnabled(true);
        setBarColor(ContextCompat.getColor(IntroScreen.this, R.color.white));
        setSeparatorColor(ContextCompat.getColor(IntroScreen.this, R.color.colorPrimaryLight));
        setColorDoneText(ContextCompat.getColor(IntroScreen.this, R.color.colorPrimary));
        setColorSkipButton(ContextCompat.getColor(IntroScreen.this, R.color.colorPrimary));
        setNextArrowColor(ContextCompat.getColor(IntroScreen.this, R.color.colorPrimary));
        setIndicatorColor(ContextCompat.getColor(IntroScreen.this, R.color.colorPrimary),ContextCompat.getColor(IntroScreen.this, R.color.iconsLight));
    }
    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        if (myAppPrefsManager.isFirstTimeLaunch()) {
            startActivity(new Intent(IntroScreen.this, MainActivity.class));
            finish();
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);
        }
        else {
            finish();
        }
    }
    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        if (myAppPrefsManager.isFirstTimeLaunch()) {
            startActivity(new Intent(IntroScreen.this, MainActivity.class));
            finish();
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);
        }
        else {
            finish();
        }
    }
    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        String languageCode = ConstantValues.LANGUAGE_CODE;
        if ("".equalsIgnoreCase(languageCode))
            languageCode = ConstantValues.LANGUAGE_CODE = "en";
        super.attachBaseContext(LocaleHelper.wrapLocale(newBase, languageCode));
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (myAppPrefsManager.isFirstTimeLaunch()) {
            startActivity(new Intent(IntroScreen.this, MainActivity.class));
            finish();
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);
        }
        else {
            finish();
        }
    }
}