package com.exitotechnology.demoapp.interfaces;

public interface RazorPayCallback {
    void onSuccess(String paymentId);
    void onError(int code, String response);
}
